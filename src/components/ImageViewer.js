import React from 'react';
import { Modal } from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export class ImageView extends React.Component {
    render(){
        return (
            <Modal visible={this.props.visible} transparent={true}>
                <ImageViewer
                    imageUrls={this.props.images}
                    enableSwipeDown={false}
                    renderHeader={() => <TouchableOpacity onPress={this.props.closeImage} style={styles.close_btn} activeOpacity={0.7}>
                        <Icon name="arrow-back" size={36} color="white" />
                    </TouchableOpacity>}
                    renderIndicator={() => null}
                />
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    close_btn: {
        width: 50,
        height: 50,
        position: "absolute",
        top: 20,
        left: 5,
        zIndex: 99,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
});