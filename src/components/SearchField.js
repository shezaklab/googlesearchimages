import React from 'react';
import { StyleSheet, Text, View, TextInput, Image, TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export class SearchField extends React.Component {

    _onPressButton(){
        this._textInput.blur();
        this.props.onSearch();
    }

    searchSubmit(val){
        this.props.onSearch();
    }

    changeText(text){
        this.props.searchText(text);
    }

    render(){
        return (
            <View style={styles.box}>
                <View style={styles.box_inner}>
                    <View style={styles.input_box}>
                        <TextInput
                            style={styles.input}
                            placeholder={"Введите запрос"}
                            placeholderTextColor={"#BDBDBD"}
                            ref={textInput => {this._textInput = textInput}}
                            keyboardType={"default"}
                            returnKeyType={"search"}
                            onSubmitEditing={this.searchSubmit.bind(this)}
                            autoCorrect={false}
                            onChangeText={this.changeText.bind(this)}
                        />
                    </View>
                    <TouchableHighlight onPress={this._onPressButton.bind(this)} style={styles.search_btn} underlayColor="#2979FF">
                        <Icon name="search" size={32} color="white" />
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    box: {
        backgroundColor: "white",
        position: "absolute",
        top: 30,
        left: 10,
        right: 10,
        height: 50,
        zIndex: 20,
        borderRadius: 10,
        elevation:4,
        shadowOffset: { width: 0, height: 2 },
        shadowColor: "black",
        shadowOpacity: 0.4,
        shadowRadius: 2,
    },
    box_inner: {
        height: 50,
        borderRadius: 10,
        overflow: "hidden",
    },
    input_box: {
        zIndex: 1,
    },
    input: {
        height: 50,
        borderColor: 'white',
        borderWidth: 0,
        paddingLeft: 15,
        paddingRight: 60,
        fontFamily: "Roboto",
        fontSize: 16,
        color: "black",
    },
    search_btn: {
        position: "absolute",
        zIndex: 3,
        top:0,
        right: 0,
        backgroundColor: "#2962FF",
        height: 50,
        width: 50,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    }
});