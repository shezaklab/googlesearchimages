import React from 'react';
import { StyleSheet, Text, View, TextInput, Image, TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export class ErrorScreen extends React.Component {
    render(){
        return (
            <View style={styles.error}>
                <Icon name={this.props.icon} size={42} color={this.props.color} />
                <View style={styles.error_title}>
                    <Text style={[styles.error_title_text, {color: this.props.color}]}>{this.props.text}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    error: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: 5,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#EEEEEE',
    },
    error_title: {
        marginTop: 10,
    },
    error_title_text: {
        fontFamily: "Roboto",
        fontSize: 16,
        lineHeight: 21,
    }
});