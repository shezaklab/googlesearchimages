import React from 'react';
import { StyleSheet, View, FlatList, Image, Text, Dimensions, TouchableHighlight, TouchableOpacity, ActivityIndicator} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const {height, width} = Dimensions.get('window');

class ImageItem extends React.Component {

    _onPressButton(){
        if(!this.props.loading && !this.props.error){
            this.props.openImage({
                url: this.props.image,
                //freeHeight: true,
                props: {
                    header: "Hello world"
                }
            });
        }
    }

    render() {

        var activeOpacity = 0.7;
        if(this.props.loading || this.props.error){
            activeOpacity = 1;
        }

        return (
            <TouchableOpacity onPress={this._onPressButton.bind(this)} style={styles.grid} activeOpacity={activeOpacity}>
                <View style={styles.grid_inner}>
                    <View style={styles.image_view}>
                        {this.props.loading?(
                            <View style={styles.image_loading}>
                                <ActivityIndicator size="large" color="#616161" />
                            </View>
                        ):null}
                        {this.props.error?(
                            <View style={styles.image_error}>
                                <Icon name="error" size={40} color="white" />
                                <View style={styles.error_title}>
                                    <Text style={styles.error_title_text}>Ошибка загрузки</Text>
                                </View>
                            </View>
                        ):null}
                        <Image
                            style={styles.image}
                            onLoad={() => {this.props.onLoad(this.props.id)}}
                            onError={() => {this.props.onError(this.props.id)}}
                            source={{uri: this.props.image}}
                        />
                    </View>
                    <View style={styles.title}>
                        <Text
                            style={styles.title_text}
                            numberOfLines={1}
                            ellipsizeMode={"tail"}
                        >{this.props.title}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

export class ImagesList extends React.Component {

    _renderItem = ({item}) => (
        <ImageItem
          key={item.key}
          id={item.key}
          title={item.title}
          image={item.image}
          loading={item.loading}
          error={item.error}
          onError={this.props.onError}
          onLoad={this.props.onLoad}
          openImage={this.props.openImage}
        />
    );

    render(){
        return (
            <FlatList
                data={this.props.data}
                initialNumToRender={30}
                windowSize={3}
                renderItem={this._renderItem}
                style={styles.list}
                numColumns={2}
                ListHeaderComponent={<View style={styles.top}></View>}
                ListFooterComponent={<ListFoot loadMore={this.props.loadMore} more={this.props.more} />}
            />
        );
    }
}

class ListFoot extends React.Component {
    render() {
        return (
            <View style={styles.foot}>
                {this.props.more?(
                    <TouchableHighlight onPress={this.props.loadMore} style={styles.foot_btn} underlayColor="#2979FF">
                        <Text style={styles.foot_btn_text}>Загрузить ещё</Text>
                    </TouchableHighlight>
                ):null}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    top: {
        height: 100,
    },
    list: {
        flex: 1,
        zIndex: 1,
    },
    grid: {
        width: width / 2 - 20,
        backgroundColor: "white",
        borderRadius: 10,
        elevation:4,
        shadowOffset: { width: 0, height: 2 },
        shadowColor: "black",
        shadowOpacity: 0.2,
        shadowRadius: 2,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 20,
    },
    grid_inner: {
        borderRadius: 10,
        overflow: "hidden",
    },
    image_view: {
        backgroundColor: "#FAFAFA",
        width: width / 2 - 20,
        height: width / 2 - 20,
        overflow: "hidden",
    },
    image: {
        width: width / 2 - 20,
        height: width / 2 - 20,
        resizeMode: Image.resizeMode.cover,
        zIndex: 1,
    },
    image_loading: {
        width: width / 2 - 20,
        height: width / 2 - 20,
        position: "absolute",
        zIndex: 5,
        backgroundColor: "#FAFAFA",
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image_error: {
        width: width / 2 - 20,
        height: width / 2 - 20,
        position: "absolute",
        zIndex: 5,
        backgroundColor: "#B71C1C",
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    error_title: {
        marginTop: 10,
    },
    error_title_text: {
        fontFamily: "Roboto",
        fontSize: 12,
        color: "white",
    },
    title: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 10,
    },
    title_text: {
        fontSize: 16,
        lineHeight: 21,
        fontWeight: '400',
        fontFamily: "Roboto",
    },
    foot: {
        paddingLeft: 10,
        paddingRight: 10,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 20,
    },
    foot_btn: {
        width: 200,
        height: 50,
        backgroundColor: "#2962FF",
        borderRadius: 10,
        elevation:4,
        shadowOffset: { width: 0, height: 2 },
        shadowColor: "black",
        shadowOpacity: 0.2,
        shadowRadius: 2,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    foot_btn_text: {
        fontFamily: "Roboto",
        fontSize: 16,
        color: "white",
    }
});