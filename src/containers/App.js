import React from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { SearchField } from '../components/SearchField';
import { ImagesList } from '../components/ImagesList';
import { ErrorScreen } from '../components/ErrorScreen';
import { ImageView } from '../components/ImageViewer';

import * as searchActions from '../actions/SearchActions';
import * as viewerActions from '../actions/ViewerActions';

class App extends React.Component {
    render(){

        return (
            <View style={styles.main}>
                <ImageView visible={this.props.visible} images={this.props.images} closeImage={this.props.viewerActions.closeImage} />
                <LinearGradient colors={['#EEEEEE', 'rgba(238,238,238,0)']} style={styles.linearGradient}></LinearGradient>
                {this.props.loading?(
                    <View style={styles.loading}>
                        <ActivityIndicator size="large" color="#616161" />
                    </View>
                ):null}
                {this.props.error?(
                    <ErrorScreen icon={"error"} text={"Произошла ошибка"} color={"red"} />
                ):null}
                {!this.props.results?(
                    <ErrorScreen icon={"warning"} text={"Ничего не найдено"} color={"#616161"} />
                ):null}
                <SearchField searchText={this.props.searchActions.searchText} onSearch={this.props.searchActions.fetchPosts} />
                <ImagesList
                    data={ this.props.posts }
                    loadMore={this.props.searchActions.loadMore}
                    more={this.props.more}
                    onLoad={this.props.searchActions.onLoad}
                    onError={this.props.searchActions.onError}
                    openImage={this.props.viewerActions.openImage}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: '#EEEEEE',
    },
    linearGradient: {
        height: 130,
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        zIndex: 5,
    },
    loading: {
        position: "absolute",
        top: 0,
        left: 0,
        backgroundColor: '#EEEEEE',
        right: 0,
        bottom: 0,
        zIndex: 4,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

function mapStateToProps(state) {
    return {
        posts: state.search.posts,
        loading: state.search.loading,
        search: state.search.search,
        more: state.search.more,
        error: state.search.error,
        results: state.search.results,
        visible: state.viewer.visible,
        images: state.viewer.images,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        searchActions: bindActionCreators(searchActions, dispatch),
        viewerActions: bindActionCreators(viewerActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);