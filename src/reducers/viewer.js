import {
  SHOW_IMAGE,
  CLOSE_IMAGE
} from '../constants/Viewer';

const initialState = {
  visible: false,
  images: []
};

const search = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_IMAGE:
      return {
        ...state,
        visible: true,
        images: action.payload,
      }
    case CLOSE_IMAGE:
      return {
          ...state,
          visible: false,
          images: [],
      }
    default:
      return state
  }
}

export default search;