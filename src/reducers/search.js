import {
  FETCH_DATA,
  REQUEST_DATA,
  SEARCH_TEXT,
  LOAD_MORE,
  LOAD_MORE_START,
  RESULTS_NO,
  LOAD_MORE_NO,
  NETWORK_ERROR,
  IMAGE_ERROR,
  IMAGE_LOAD,
} from '../constants/Search';

const initialState = {
  loading: false,
  error: false,
  page: 1,
  posts: [],
  search: "",
  more: false,
  results: true,
};

const search = (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_TEXT:
      return {
        ...state,
        search: action.payload,
      }
    case FETCH_DATA:
      return {
        ...state,
        posts: action.payload,
        loading: false,
        error: false,
        more: true,
      }
    case REQUEST_DATA:
      return {
        ...state,
        loading: true,
        error: false,
        results: true,
      }
    case LOAD_MORE_START:
      return {
        ...state,
        page: state.page + 10,
        loading: true,
        error: false,
        results: true,
      }
    case LOAD_MORE:
      return {
        ...state,
        posts: [
          ...state.posts,
          ...action.payload
        ],
        loading: false,
        error: false,
      }
    case RESULTS_NO:
      return {
        ...state,
        posts: [],
        loading: false,
        more: false,
        error: false,
        results: false,
      }
    case NETWORK_ERROR:
      return {
        ...state,
        error: true,
      }
    case LOAD_MORE_NO:
      return {
        ...state,
        more: false,
        loading: false,
      }
    case IMAGE_ERROR:
      const errorItems = state.posts.map(item => {
        if(item.key === action.payload){
          return { ...item, loading: false, error: true }
        }
        return item
      })
      return {
        ...state,
        posts: errorItems
      }
    case IMAGE_LOAD:
      const loadItems = state.posts.map(item => {
        if(item.key === action.payload){
          return { ...item, loading: false }
        }
        return item
      })
      return {
        ...state,
        posts: loadItems
      }
    default:
      return state
  }
}

export default search;