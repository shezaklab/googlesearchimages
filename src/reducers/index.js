import { combineReducers } from 'redux';
import search from './search';
import viewer from './viewer';

export default combineReducers({
  search,
  viewer
})