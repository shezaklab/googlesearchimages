import {
    SHOW_IMAGE,
    CLOSE_IMAGE
} from '../constants/Viewer';

export const openImage = image => dispatch => {
    console.log(image);
    dispatch({type: SHOW_IMAGE, payload: [image]})
}

export const closeImage = info => dispatch => {
    dispatch({type: CLOSE_IMAGE})
}