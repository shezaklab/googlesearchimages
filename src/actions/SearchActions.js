import {
  FETCH_DATA,
  SEARCH_TEXT,
  REQUEST_DATA,
  LOAD_MORE,
  LOAD_MORE_START,
  RESULTS_NO,
  LOAD_MORE_NO,
  NETWORK_ERROR,
  IMAGE_ERROR,
  IMAGE_LOAD,
} from '../constants/Search';

export const fetchPosts = search => (dispatch, getState) => {
  let current = getState().search;
  let search = current.search;
  let loading = current.loading;

  if(search!=="" && !loading){
    dispatch({type: REQUEST_DATA});

    return fetch(`https://www.googleapis.com/customsearch/v1?q=${encodeURIComponent(search)}&cx=010202561478679623980%3Avyrylrbwsv4&key=AIzaSyDOLkG-DmiCyaiJm3UgRgrBBkL1kj-Icx8`, {
      mode: "no-cors",
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    }).then(response => {
        if (response.ok) {
          response.json().then(json => {
            let items = [];
            if(json.items){
              json.items.map((item, index) => {
                var data = {
                  key: index,
                  title: item.title || "",
                  loading: true,
                  error: false,
                };

                if(item.pagemap && item.pagemap.cse_image && item.pagemap.cse_image[0]){
                  data.image = item.pagemap.cse_image[0].src || "";
                }

                items.push(data);
              });

              dispatch({type: FETCH_DATA, payload: items})
            } else {
              dispatch({type: RESULTS_NO})
            }
          });
        }
      })
      .catch((error) => {
        dispatch({type: NETWORK_ERROR});
      });
  }
}

export const loadMore = index => (dispatch, getState) => {

  dispatch({type: LOAD_MORE_START});

  let current = getState().search;
  let page = current.page;
  let search = current.search;

  dispatch({type: REQUEST_DATA});

  return fetch(`https://www.googleapis.com/customsearch/v1?q=${encodeURIComponent(search)}&cx=010202561478679623980%3Avyrylrbwsv4&key=AIzaSyDOLkG-DmiCyaiJm3UgRgrBBkL1kj-Icx8&start=${page}`, {
    mode: "no-cors",
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
  }).then(response => {
    if (response.ok) {
      response.json().then(json => {
        let items = [];
        if(json.items){
          json.items.map((item, index) => {
            var data = {
              key: index + page - 1,
              title: item.title || "",
              loading: true,
              error: false,
            };

            if(item.pagemap && item.pagemap.cse_image && item.pagemap.cse_image[0]){
              data.image = item.pagemap.cse_image[0].src || "";
            }

            items.push(data);
          });

          dispatch({type: LOAD_MORE, payload: items})
        } else {
          dispatch({type: LOAD_MORE_NO})
        }
      });
    }
  }).catch((error) => {
    dispatch({type: NETWORK_ERROR});
  });
}

export const searchText = search => dispatch => {
  dispatch({type: SEARCH_TEXT, payload: search})
}

export const onError = id => dispatch => {
  console.log("error: ", id);
  dispatch({type: IMAGE_ERROR, payload: id})
}

export const onLoad = id => dispatch => {
  console.log("loaded: ", id);
  dispatch({type: IMAGE_LOAD, payload: id})
}