## Google Search React-Native/Redux Application


### Application functions:
* Search (by keyboard & search btn)
* List of images with title
* Network error catch
* Images loading error status


### Screenshots (iOS):
* [Start screen](https://ibb.co/bSxZvp)
* [Images list](https://ibb.co/nK2Zvp)
* [Images list with load more button](https://ibb.co/ecRG89)
* [Image viewer](https://ibb.co/eZXCgU)


### Screenshots (Android):
* [Start screen](https://ibb.co/h5U8MU)
* [Images list with load more button](https://ibb.co/gCFhgU)
* [Image viewer](https://ibb.co/hWxv1U)